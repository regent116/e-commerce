/* eslint-disable import/prefer-default-export */
export const setArrayLimit = (arr, limit) => {
	if (arr.length > limit) {
		return arr.splice(0, limit);
	}
	return arr;
};
