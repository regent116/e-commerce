import {createSlice} from "@reduxjs/toolkit";

export const categorySlice = createSlice({
    name: "category",
    initialState: { value: {name: "hello"}},
    reducers: {
        getCategory: (state, action) => {
            state.value = action.payload;
        }
    }
})

const { actions, reducer } = categorySlice;

// export individual action creator functions
export const { getCategory } = actions;

// often the reducer is a default export, but that doesn't matter
export default reducer;