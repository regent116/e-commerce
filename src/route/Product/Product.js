import React, { PureComponent } from 'react';
import Img from '../../images/sweater2.png';
import GalleryImage from '../../component/GalleryImage';
import GalleryThumbnail from '../../component/GalleryThumbnail';
import Price from '../../component/Price';
import Size from '../../component/Size';
import AddToCartBtn from '../../component/AddToCartBtn';
import './Product.style.css';



export default class Product extends PureComponent {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	render() {
		return (
			<div className="product-page-container">
			<div className="product-page-wrapper">
				<div>
				<GalleryThumbnail/>
				<GalleryThumbnail/>
				<GalleryThumbnail/>	
				</div>
				<div><GalleryImage/></div>
				<div className="product-actions-wrapper">
					<div className="name-wrapper">
					<div className="name-wrapper__brand">
						Apollo
					</div>
					<div className="name-wrapper__item">
						Running Short
					</div>
					</div>
					<div className="size-wrapper">
					<div className="size-wrapper__title">
						Size:
					</div>
					<div className="size-wrapper__blocks-wrapper">
						<Size value='xs' inStock='false' active='false'/>
						<Size value='s' inStock='true' active='true'/>
						<Size value='m' inStock='true' active='false'/>
						<Size value='l' inStock='true' active='false'/>
					</div>
					</div>
					<div className="price-wrapper">
						<div className="price-wrapper__title">price: </div>
						<div className="price-wrapper__price"><Price price='50'/></div>
					</div>
					<div className="add-to-cart-wrapper">
						<AddToCartBtn active='true'/>
					</div>
					<div className="desription-wrapper">
						<div className="desription-wrapper__content">Find stunning women's cocktail dresses and party dresses. Stand out in lace and metallic cocktail dresses and party dresses from all your favorite brands.</div>
					</div>
				</div>
			</div>
			</div>
		);
	}
}
