import React, { PureComponent } from 'react';
import CategoryItem from '../../component/CategoryItem';
import './Categories.style.css';
import { Link } from 'react-router-dom';
import {useCategories} from '../../model/model'




export default class Categories extends PureComponent {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		useCategories().then((res)=>{
			console.log(res)
		});
	}

	render() {
		return (
			<div className="product-container">
				<h1>Categories Page</h1> 
			
				
			<div className="product-wrapper">
			<CategoryItem />
			<CategoryItem />
			<CategoryItem />
			<CategoryItem />
			<CategoryItem />
			<CategoryItem />
			<CategoryItem />
			<CategoryItem />
			</div>
			</div>
		);
	}
}