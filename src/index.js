import React from 'react';
import { render } from 'react-dom';
import App from './component/App';
import {configureStore} from '@reduxjs/toolkit';
import {Provider} from "react-redux";
import categoryReducer from "./features/categories"

const store = configureStore({
    reducer: {category: categoryReducer}
})

const app = document.createElement('div');
document.body.appendChild(app);

render(
    <Provider store={store}>
        <App />
    </Provider>
    , app);
module.hot.accept();
