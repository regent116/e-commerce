import gsap, { Power3 } from 'gsap';

export const animateIntro = (elem1, elem2, elem3, elem4, elem5) => {
	gsap
		.timeline()
		.from(elem1, {
			duration: 2,
			opacity: 0,
			backgroundColor: 'pink',
			minHeight: 500,
			ease: Power3.easeInOut
		})
		.from(
			elem2,
			{
				duration: 2,
				yPercent: 20,
				opacity: 0,
				delay: 0,
				ease: Power3.easeInOut
			},
			'-=2'
		)
		.from(
			elem3,
			{
				duration: 3,
				yPercent: 20,
				opacity: 0,
				delay: 1,
				ease: Power3.easeInOut
			},
			'-=3'
		)
		.from(
			elem4,
			{
				duration: 3,
				minHeight: 100,
				opacity: 0,
				delay: 1,
				ease: Power3.easeInOut
			},
			'-=3'
		)
		.from(
			elem5,
			{
				duration: 4,
				opacity: 0,
				xPercent: 20,
				ease: Power3.easeInOut
			},
			'-=3'
		);
};

export const animatePreviousJoke = (elem) => {
	gsap.from(elem, {
		duration: 2,
		yPercent: 20,
		opacity: 0,
		delay: 0,
		ease: Power3.easeInOut
	});
};

export const animateJokeContainer = (elem, message) => {
	gsap
		.timeline()
		.to(elem, {
			opacity: 0,
			height: 0,
			text: '',
			yPercent: -10
		})
		.to(elem, {
			duration: 2,
			opacity: 1,
			height: 100,
			text: message,
			ease: Power3.easeInOut
		});
};

export const animatePreviousJokeContainer = (elem) => {
	gsap.fromTo(
		elem,
		{
			opacity: 0,
			height: 0,
			yPercent: -10
		},
		{
			duration: 5,
			opacity: 1,
			height: 100,
			ease: Power3.easeInOut
		}
	);
};
