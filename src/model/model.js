import {ApolloClient, InMemoryCache, useQuery, gql} from '@apollo/client';

const client = new ApolloClient({
    uri: "http://localhost:4000/",
    cache: new InMemoryCache()
})


export const useCategories = () => {
    return client.query({
      query: gql`
      {
        categories {
          name
        }
    }
    `
    })
    .then(result => {
        return result;
    });
}

