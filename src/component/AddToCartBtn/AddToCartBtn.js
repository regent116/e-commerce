import React, { PureComponent } from 'react';
import './AddToCartBtn.style.css';

export default class AddToCartBtn extends PureComponent {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	render() {
        const {active } = this.props;
		return (
            <div className={`cart-btn ${active === 'false' ? 'cart-btn-inActive' : ''}`}>
                add to cart
            </div>
		);
	}
}