import React, { PureComponent } from 'react';
import './NavItem.style.css';

export default class NavItem extends PureComponent {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	render() {
        const { name, active } = this.props;
		return (
            <div className={`nav-item ${active === 'true' ? 'active' : ''}`}>
                {name}
            </div>
		);
	}
}