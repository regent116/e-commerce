import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import './App.style.css';
import {NavigationBar} from '../NavigationBar';
import Categories from '../../route/Categories';
import Product from '../../route/Product';
import FooterSection from '../FooterSection';

import { animateIntro } from '../../Animation.js';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Redirect } from 'react-router';

export default class App extends PureComponent {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	render() {
		return (
			<Router>	
				<Helmet>
					<meta name="viewport" content="width=device-width, initial-scale=1.0" />
				</Helmet>
				<NavigationBar/>
				<Routes>
				<Route path="/" exact element={<Categories/>}/>
				<Route path="/product" element={<Product/>}/>
				</Routes>
				<FooterSection />
			</Router>
		);
	}
}
