import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import './Button.style.css';

export default class Button extends PureComponent {
	render() {
		const { value, submit, hoverClassName } = this.props;

		return (
			<button type="button" className={hoverClassName} onClick={submit}>
				<span>{value}</span>
			</button>
		);
	}
}
Button.propTypes = {
	value: PropTypes.string,
	submit: PropTypes.func,
	hoverClassName: PropTypes.string
};

Button.defaultProps = {
	value: null,
	submit: null,
	hoverClassName: null
};
