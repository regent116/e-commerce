import React, { PureComponent } from 'react';
import Cart from '../../images/cart.svg';
import './CartIcon.style.css';

export default class CartIcon extends PureComponent {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	render() {
        const { name, active } = this.props;
		return (
			<div className="cart-icon action-items">
				<img src={Cart} />
			</div>
		);
	}
}