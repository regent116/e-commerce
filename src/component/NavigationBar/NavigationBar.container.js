import {connect} from "react-redux";
import { PureComponent } from 'react';
import NavigationBar from "./NavigationBar.component";
import { getCategory } from "../../features/categories";

export const mapStateToProps = (state) => ({
    cat: state.category.value
});

const mapDispatchToProps = () => ({});

export class NavigationBarContainer extends PureComponent {
    containerProps(){
        const{
            cat
        } = this.props
        return {
            cat
        };
    }

    

    render() {
        return (
            <NavigationBar {...this.containerProps()}/>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavigationBarContainer); 