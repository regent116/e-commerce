import React, { PureComponent } from 'react';
import './NavigationBar.style.css';
import NavItem from '../NavItem';
import CartIcon from '../CartIcon';
import Logo from '../../images/logo.svg';
import Checkout from '../../images/checkout_cropped.svg';
import { Link } from 'react-router-dom';

export  class NavigationBar extends PureComponent {

	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	render() {
		const {cat} = this.props;
		console.log(cat);
		return (
            <div className="navbar-container">
				<div className="navbar-wrapper">
					<div className="nav-items-wrapper">
						<NavItem name={cat} active="true" />
						<NavItem name="Men" active="false" />
						<NavItem name="Kids" active="false" />
					</div>
					<Link to="/">
					<div className="logo"><img src={Logo}/></div>
					</Link>
					<div className="actions">
						<div className="checkout action-items"><img src={Checkout} width="30"/></div>
						<CartIcon />
					</div>
				</div>
            </div>
		);
	}
}

export default NavigationBar;
