import React, { PureComponent } from 'react';
import Sweater from '../../images/sweater2.png';
import './GalleryThumbnail.style.css';

export default class GalleryThumbnail extends PureComponent {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	render() {
		return (
			<div className="gallery-thumbnail">
				<img src={Sweater} />
			</div>
		);
	}
}