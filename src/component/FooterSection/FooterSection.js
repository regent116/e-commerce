import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './FooterSection.style.css';

export default class FooterSection extends PureComponent {
    render() {
        return (
            <section className="footer-wrapper">
                <div className="footer-links">
                    <div className="footer-column-title">Navigation</div>
                    <div className="footer-column-content">
                        <ul>
                            <li className="link-item"><a>Popular search</a></li>
                            <li className="link-item"><a>Write to us</a></li>
                            <li className="link-item"><a>About</a></li>
                        </ul>
                    </div>
                </div>
                <div className="footer-links">
                    <div className="footer-column-title">My Profile</div>
                    <div className="footer-column-content">
                        <ul>
                            <li className="link-item"><a>Profile</a></li>
                        </ul>
                    </div>
                </div>
                <div className="footer-links">
                    <div className="footer-column-title">Quick Contact</div>
                    <div className="footer-column-content">
                        <div className="quick-contact-wrapper">
                            <div className="quick-contact-wrapper__item">
                       <div className="quick-contact-title-wrapper">
                           <div className="quick-contact-icon">0</div>
                           <div className="quick-contact-title">Adress</div>
                        </div>
                        <div className="quick-contact-content">Ziepniekkalns Rīga</div>
                        </div>
                        </div>
                        
                        <div className="quick-contact-wrapper">
                        <div className="quick-contact-wrapper__item">
                        <div className="quick-contact-title-wrapper">
                           <div className="quick-contact-icon">3</div>
                           <div className="quick-contact-title">Email</div>
                           </div>
                           <div className="quick-contact-content">pavels@concords.lv</div>
                           </div>
                           </div>
                           <div className="quick-contact-wrapper">
                           <div className="quick-contact-wrapper__item">
                        <div className="quick-contact-title-wrapper">
                           <div className="quick-contact-icon">"</div>
                           <div className="quick-contact-title">Number</div>
                       </div>
                       <div className="quick-contact-content">27122753</div>
                       </div>
                       </div>   
                    </div>
                </div>
                <div className="footer-bottom">
                    Copyright 2022
                </div>
            </section>
        );
    }
}

FooterSection.propTypes = {

};
