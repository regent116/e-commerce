import React, { PureComponent } from 'react';
import './Size.style.css';

export default class Size extends PureComponent {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	render() {
        const { value, inStock, active } = this.props;
        const isEmpty = inStock === 'false' ? 'empty' : '';
        const isActive = active === 'true' ? 'size-active' : '';
		return (
            <div className={`size-item ${isEmpty} ${isActive}`}>
                {value}
            </div>
		);
	}
}