import React, { PureComponent } from 'react';
import Price from '../../component/Price';
import { Link } from 'react-router-dom';
import './CategoryItem.style.css';

export default class CategoryItem extends PureComponent {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	render() {
        const { price } = this.props;
		return (
            <div className="product-item-wrapper">
			<Link to="/product">
			<div className="product-thumbnail">
				<img className="product-image" src="https://images.unsplash.com/photo-1488190211105-8b0e65b80b4e?w=500&h=500&fit=crop" alt="person writing in a notebook beside by an iPad, laptop, printed photos, spectacles, and a cup of coffee on a saucer"/>
			</div>
			</Link>
			<div className="product-item-name">Apollo Running Short</div>
			<div className="product-item-price"><Price price='50'/></div>
		    </div>
		);
	}
}