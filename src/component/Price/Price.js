import React, { PureComponent } from 'react';

export default class Price extends PureComponent {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	render() {
        const { price } = this.props;
		return (
			<div>
				{ price }$
			</div>
		);
	}
}