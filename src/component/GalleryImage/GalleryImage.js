import React, { PureComponent } from 'react';
import Sweater from '../../images/sweater2.png';
import './GalleryImage.style.css';

export default class GalleryImage extends PureComponent {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	render() {
		return (
			<div className="GalleryImage">
				<img src={Sweater} />
			</div>
		);
	}
}